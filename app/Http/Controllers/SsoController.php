<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class SsoController extends Controller
{
    public function redirectToProvider()
    {
        return Socialite::driver('cognito')->redirect();
    }

    public function callBack(){
        $cognitoUser = Socialite::driver('cognito')->stateless()->user();
        dd($cognitoUser);
    }
}
